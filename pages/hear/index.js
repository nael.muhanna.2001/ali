import axios from "axios";
import { useRef, useState } from "react";
import Select from "react-select";
import {
  Form,
  SelectPicker,
  InputPicker,
  ButtonToolbar,
  Button,
  Rate,
} from "rsuite";
// عم
const amaList = [
  "الفاتحة",
  "الناس",
  "الفلق",
  "الإخلاص",
  "المسد",
  "النصر",
  "الكافرون",
  "الكوثر",
  "الماعون",
  "قريش",
  "الفيل",
  "الهمزة",
  "العصر",
  "التكاثر",
  "القارعة",
  "العاديات",
  "الزلزلة",
  "البينة",
  "القدر",
  "العلق",
  "التين",
  "الشرح",
  "الضحى",
  "الليل",
  "الشمس",
  "البلد",
  "الفجر",
  "الغاشية",
  "الأعلى",
  "الطارق",
  "البروج",
  "الإنشقاق",
  "المطففين",
  "الإنفطار",
  "التكوير",
  "عبس",
  "النازعات",
  "النبأ",
].map((item) => ({ label: item, value: item }));
// تبارك
const tabarkList = [
  "1-15 الملك",
  "16-30 الملك",
  "1-33 القلم",
  "34-56 القلم",
  "1-17 الحاقة",
  "18-25 الحاقة",
  "المعارج",
  "نوح",
  "1-13 الجن",
  "14-28 الجن",
  "المزمل",
  "1-31 المدثر",
  "32-56 المدثر",
  "القيامة",
  "الإنسان",
  "1-28 المرسلات",
  "29-50 المرسلات",
].map((item) => ({ label: item, value: item }));

export default function NewStudent({ student_data }) {
  const [name, setName] = useState("");
  const [page, setPage] = useState("");
  const [rate, setRate] = useState(1);
  const [ama, setAma] = useState("");
  const [tabark, setTabark] = useState("");
  const studentData = student_data.getData;
  console.log(studentData);
  const studentName = studentData
    .map((e) => {
      return e.name;
    })
    .map((item) => ({ label: item, value: item }));
  const submitHandle = (event) => {
    event.preventDefault();
  };
  const click = () => {
    const reqBody = {
      name: name,
      page: Number(page),
      ama: ama,
      tabark: tabark,
      rate: rate,
    };
    axios
      .post("/api/hear/new_hear", { ...reqBody })
      .then((val) => {
        setName("");
        setPage("");
        setRate(1);
        setAma("");
        setTabark("");
        alert("success!");
        console.log(val);
      })
      .catch((err) => {
        alert(err.message);
      });
  };
  const refreshPage = () => {
    document.location.reload();
  };
  return (
    <>
      <section className="max-w-4xl p-6 mx-auto bg-indigo-600 rounded-md shadow-md dark:bg-gray-800 mt-20">
        <h1 className="text-2xl  font-bold text-white capitalize dark:text-white">
          تسميع جديد
        </h1>
        <Form>
          <div className="grid grid-cols-1 gap-6 m-8 sm:grid-cols-2 ">
            {/* اسم الطالب */}
            <div>
              <div>
                <label className="text-white uppercase font-semibold ">
                  اسم الطالب
                </label>
              </div>
              <div>
                <InputPicker
                  data={studentName}
                  style={{ width: 300 }}
                  className="block w-full   border-gray-300 rounded-md text-white dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring"
                  value={name}
                  placeholder="اختر اسم الطالب"
                  onChange={(e) => {
                    setName(e);
                  }}
                />
              </div>
            </div>
            {/* عم  */}
            <div>
              <div>
                <label className="text-white uppercase font-semibold ">
                  جزء عم
                </label>
              </div>
              <div>
                <InputPicker
                  data={amaList}
                  style={{ width: 300 }}
                  className="block w-full  border-gray-300 rounded-md text-white dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring"
                  placeholder=" اختر السورة "
                  value={ama}
                  onChange={(e) => {
                    setAma(e);
                  }}
                />
              </div>
            </div>
            {/* تبارك */}
            <div>
              <div>
                <label className="text-white uppercase font-semibold ">
                  جزء تبارك
                </label>
              </div>
              <div>
                <InputPicker
                  data={tabarkList}
                  style={{ width: 300 }}
                  className="block w-full  border-gray-300 rounded-md text-white dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring"
                  placeholder=" اختر السورة "
                  value={tabark}
                  onChange={(e) => {
                    setTabark(e);
                  }}
                />
              </div>
            </div>
            {/*  أجزاء متقدمة */}
            <div>
              <Form.Group controlId="name">
                <Form.ControlLabel className="text-white dark:text-gray-200 font-bold">
                  أجزاء متقدمة
                </Form.ControlLabel>
                <Form.Control
                  name="name"
                  type="text"
                  className="block w-full text-white  border-gray-300 rounded-md  dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring"
                  placeholder="ضع رقم الصفحة"
                  value={page}
                  onChange={(e) => {
                    setPage(e);
                  }}
                />
              </Form.Group>
            </div>
            {/* تقييم الصفحة */}
            <div className="m-8">
              <div>
                <Rate
                  defaultValue={1}
                  max={3}
                  color="blue"
                  value={rate}
                  onChange={(e) => {
                    setRate(e);
                  }}
                />
              </div>
            </div>
            <div>
              <div className="m-20" style={{ direction: "ltr" }}>
                <Form.Group>
                  <ButtonToolbar>
                    <Button
                      appearance="primary"
                      className=" w-24"
                      onClick={click}
                    >
                      حفظ
                    </Button>
                    <Button
                      appearance="default"
                      className=" w-24"
                      onClick={refreshPage}
                    >
                      تحديث
                    </Button>
                  </ButtonToolbar>
                </Form.Group>
              </div>
            </div>
          </div>
        </Form>
      </section>
    </>
  );
}

export async function getStaticProps() {
  const res = await axios.get("http://localhost:3000/api/student/data");
  return {
    props: {
      student_data: res.data,
    },
  };
}
