import MainLayout from "@/components/mainLayout";
import "@/styles/globals.css";
import { CustomProvider } from "rsuite";
import "rsuite/dist/rsuite-rtl.min.css";
//import "rsuite/styles/index.less";

export default function App({ Component, pageProps }) {
  return (
    <>
      <CustomProvider rtl theme="dark">
        <MainLayout />
        <Component {...pageProps} />
      </CustomProvider>
    </>
  );
}
