import { COLUMNS } from "@/components/columns";
import Table from "@/components/table";
import axios from "axios";
import { useRef, useState, useEffect } from "react";
export default function AllStudent() {
  useEffect(() => {
    getStudentData();
  }, []);

  const [student, setStudent] = useState([]);
  const getStudentData = () => {
    fetch("/api/student/data")
      .then((res) => {
        let data = res.json();
        return data;
      })
      .then((data) => {
        const e = data.getData.map((e) => {
          return e;
        });
        setStudent(e);
      });
  };
  return (
    <>
      <Table student={student} />
      
    </>
  );
}
