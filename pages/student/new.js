import axios from "axios";
import { useRef, useState } from "react";
import Select from "react-select";
import { Form, SelectPicker, InputPicker, ButtonToolbar, Button } from "rsuite";
// عم
const amaList = [
  "الفاتحة",
  "الناس",
  "الفلق",
  "الإخلاص",
  "المسد",
  "النصر",
  "الكافرون",
  "الكوثر",
  "الماعون",
  "قريش",
  "الفيل",
  "الهمزة",
  "العصر",
  "التكاثر",
  "القارعة",
  "العاديات",
  "الزلزلة",
  "البينة",
  "القدر",
  "العلق",
  "التين",
  "الشرح",
  "الضحى",
  "الليل",
  "الشمس",
  "البلد",
  "الفجر",
  "الغاشية",
  "الأعلى",
  "الطارق",
  "البروج",
  "الإنشقاق",
  "المطففين",
  "الإنفطار",
  "التكوير",
  "عبس",
  "النازعات",
  "النبأ",
  "اختبار",
  "مختبر",
].map((item) => ({ label: item, value: item }));
// تبارك
const tabarkList = [
  "1-15 الملك",
  "16-30 الملك",
  "1-33 القلم",
  "34-56 القلم",
  "1-17 الحاقة",
  "18-25 الحاقة",
  "المعارج",
  "نوح",
  "1-13 الجن",
  "14-28 الجن",
  "المزمل",
  "1-31 المدثر",
  "32-56 المدثر",
  "القيامة",
  "الإنسان",
  "1-28 المرسلات",
  "29-50 المرسلات",
].map((item) => ({ label: item, value: item }));
// اسماء الاساتذة
const teacherNameList = [
  "نائل مهنا",
  "خالد عجور",
  "ضياء الدين أحمد",
  "أحمد أحمد",
  "جهاد شيخ زين",
  "يمان غنام",
  "بشر خولي",
  "بلال عزيزية",
  "محمد منصور",
  "حمزة مقداد",
  "محمد أبو حمدة",
  "محي الدين بصبوص",
  " مأمون مهنا",
  "مهند الزين",
].map((item) => ({ label: item, value: item }));
// الصفوف
const classNameList = [
  "الأول",
  "الثاني",
  "الثالث",
  "الرابع",
  "الخامس",
  "السادس",
  "السابع",
  "الثامن",
  "التاسع",
  "العاشر",
  "الحادي عشر",
  "الثاني عشر",
].map((item) => ({ label: item, value: item }));

export default function NewStudent() {
  // المتغيرات
  const [studentName, setStudentName] = useState("");
  const [studentClass, setStudentClass] = useState("");
  const [teacherName, setTeacherName] = useState("");
  const [studentPage, setStudentPage] = useState("");
  const [ama, setAma] = useState("");
  const [tabark, setTabark] = useState("");
  // تابع عند الضغط
  const click = () => {
    const reqBody = {
      name: studentName,
      calss: studentClass,
      teacher: teacherName,
      page: studentPage,
      ama: ama,
      tabark: tabark,
    };
    try {
      axios.post("/api/student/new_student", { ...reqBody }).then((val) => {
        setStudentName("");
        setStudentClass("");
        setTeacherName("");
        setStudentPage("");
        setAma("");
        setTabark("");
      });
    } catch (error) {
      console.log(error);
    }
  };
  const refreshPage = () => {
    document.location.reload();
  };
  return (
    <>
      <section className="max-w-4xl p-6 mx-auto bg-indigo-600 rounded-md shadow-md dark:bg-gray-800 mt-20">
        <h1 className="text-2xl  font-bold text-white capitalize dark:text-white">
          معلومات الطالب
        </h1>
        <Form>
          <div className="grid grid-cols-1 gap-6 mt-4 sm:grid-cols-2 mr-12">
            {/* اسم الطالب */}
            <div>
              <Form.Group controlId="name">
                <Form.ControlLabel className="text-white dark:text-gray-200 font-bold">
                  اسم الطالب
                </Form.ControlLabel>
                <Form.Control
                  name="name"
                  type="text"
                  className="block w-full text-white  border-gray-300 rounded-md  dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring"
                  value={studentName}
                  onChange={(e) => {
                    setStudentName(e);
                  }}
                />
              </Form.Group>
            </div>
            {/* اسم الاستاذ */}
            <div>
              <div>
                <label className="text-white uppercase font-semibold ">
                  اسم الاستاذ
                </label>
              </div>
              <div>
                <InputPicker
                  data={teacherNameList}
                  style={{ width: 300 }}
                  className="block w-full   border-gray-300 rounded-md text-white dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring"
                  value={teacherName}
                  placeholder="اختر اسم الاستاذ"
                  onChange={(e) => {
                    setTeacherName(e);
                  }}
                />
              </div>
            </div>
            {/* الصف */}
            <div>
              <div>
                <label className="text-white uppercase font-semibold ">
                  الصف
                </label>
              </div>
              <div>
                <InputPicker
                  data={classNameList}
                  style={{ width: 300 }}
                  className="block w-full  border-gray-300 rounded-md text-white dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring"
                  value={studentClass}
                  placeholder="اختر الصف"
                  onChange={(e) => {
                    setStudentClass(e);
                  }}
                />
              </div>
            </div>
            {/* رقم الصفحة */}
            <div>
              <Form.Group controlId="name">
                <Form.ControlLabel className="text-white dark:text-gray-200 font-bold">
                  رقم الصفحة
                </Form.ControlLabel>
                <Form.Control
                  name="name"
                  type="text"
                  className="block w-full text-white  border-gray-300 rounded-md  dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring"
                  value={studentPage}
                  onChange={(e) => {
                    setStudentPage(e);
                  }}
                />
              </Form.Group>
            </div>
            {/* عم  */}
            <div>
              <div>
                <label className="text-white uppercase font-semibold ">
                  جزء عم
                </label>
              </div>
              <div>
                <InputPicker
                  data={amaList}
                  style={{ width: 300 }}
                  className="block w-full  border-gray-300 rounded-md text-white dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring"
                  placeholder=" اختر السورة "
                  value={ama}
                  onChange={(e) => {
                    setAma(e);
                  }}
                />
              </div>
            </div>
            {/* تبارك */}
            <div>
              <div>
                <label className="text-white uppercase font-semibold ">
                  جزء تبارك
                </label>
              </div>
              <div>
                <InputPicker
                  data={tabarkList}
                  style={{ width: 300 }}
                  className="block w-full  border-gray-300 rounded-md text-white dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring"
                  placeholder=" اختر السورة "
                  value={tabark}
                  onChange={(e) => {
                    setTabark(e);
                  }}
                />
              </div>
            </div>
          </div>
          {/* button */}
          <div className="m-20" style={{ direction: "ltr" }}>
            <Form.Group>
              <ButtonToolbar>
                <Button appearance="primary" className=" w-24" onClick={click}>
                  حفظ
                </Button>
                <Button
                  appearance="default"
                  className=" w-24"
                  onClick={refreshPage}
                >
                  تحديث
                </Button>
              </ButtonToolbar>
            </Form.Group>
          </div>
        </Form>
      </section>
    </>
  );
}
