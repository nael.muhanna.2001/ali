import { images } from "@/next.config";
import MyStyle from "../styles/Index.module.css";
function cssTest() {
  return (
    <div>
      <div
        style={{
          margin: "auto",
          marginTop: "20%",

          padding: "40px",
          textAlign: "center",
          width: "33.3333333%",
          fontSize: "80px",
        }}
        className=" bg-lime-800"
      >
        <a href="https://elzero.org" className=" text-white font-bold">
          Elzero
        </a>
      </div>
      <div className=" bg-lime-600 h-5 w-1/3 m-auto"></div>
    </div>
  );
}
export default cssTest;
