import { COLUMNS } from "@/components/columns";
import axios from "axios";
import { useMemo } from "react";
import { useTable } from "react-table";
import { Table, Button } from "rsuite";
import jsPDF from "jspdf";
import "../../public/font/Amiri-Bold-bold";
import autoTable from "jspdf-autotable";
const { Column, HeaderCell, Cell } = Table;
export default function AllStudent({ point }) {
  const pointSura = point.getData;
  const columns = [
    {
      title: "اسم السورة",
      field: "name",
    },
    {
      title: " النقاط",
      field: "point",
    },
  ];
  function downloadPdf() {
    const doc = new jsPDF();
    doc.autoTable({
      head: [["نقاط السورة", "اسم السورة "]],
      body: pointSura.map((col) => {
        return [[col.point], [col.name]];
      }),
      useCss: true,
      theme: "grid",
      headStyles: {
        font: "Amiri-Bold",
        halign: "right",
      }, // For Arabic text in the table head
      bodyStyles: { font: "Amiri-Bold", halign: "right" }, // For Arabic text in the table body
    });
    doc.save("point.pdf");
  }
  return (
    <div className="m-24 pr-72">
      <Table
        height={500}
        width={800}
        data={pointSura}
        onRowClick={(rowData) => {
          console.log(rowData);
        }}
      >
        <Column width={400} align="center">
          <HeaderCell className="text-base text-white"> اسم السورة</HeaderCell>
          <Cell dataKey="name" className="text-base text-white" />
        </Column>

        <Column width={400} align="center">
          <HeaderCell className="text-base text-white">النقاط </HeaderCell>
          <Cell dataKey="point" className="text-base text-white" />
        </Column>
      </Table>
      <div className="m-8 pr-72">
        <Button appearance="primary" onClick={downloadPdf}>
          convert to pdf
        </Button>
      </div>
    </div>
  );
}

export async function getStaticProps() {
  const res = await axios.get("http://localhost:3000/api/condition/getPoint");
  const studentData = res;

  return {
    props: {
      point: res.data,
    },
  };
}
