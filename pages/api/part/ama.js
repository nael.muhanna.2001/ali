import Db from "@/utils/db";
async function GetStudentData(req, res) {
  const client = await Db();
  const db = client.db("ali");
  const collection = db.collection("student");
  const student_ama = await collection.find({ ama: "", tabark: "" }).toArray();
  let count = 0;
  const page = student_ama.map((e) => {
    if (e.page > 40 && e.page < 100) {
      count++;
    }
  });
  res.status(200).json(count);
}
export default GetStudentData;
