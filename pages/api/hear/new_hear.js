import Db from "@/utils/db";

export default async function handler(req, res) {
  const client = await Db();
  const db = client.db("ali");
  let collection = db.collection("point_ama_tabark");
  const date = new Date();
  const start = new Date();
  start.setHours(3, 0, 0, 0);
  start.setFullYear(2023, 5, 4);
  const end = new Date();
  end.setHours(3, 0, 0, 0);
  end.setFullYear(2023, 5, 11);
  const week = [
    {
      start: start.setFullYear(2023, 5, 4),
      end: end.setFullYear(2023, 5, 11),
      number: "week0",
    },
    {
      start: start.setFullYear(2023, 5, 11),
      end: end.setFullYear(2023, 5, 18),
      number: "week1",
    },
    {
      start: start.setFullYear(2023, 5, 18),
      end: end.setFullYear(2023, 5, 25),
      number: "week2",
    },
    {
      start: start.setFullYear(2023, 5, 25),
      end: end.setFullYear(2023, 6, 2),
      number: "week3",
    },
    {
      start: start.setFullYear(2023, 6, 2),
      end: end.setFullYear(2023, 6, 9),
      number: "week4",
    },
    {
      start: start.setFullYear(2023, 6, 9),
      end: end.setFullYear(2023, 6, 16),
      number: "week5",
    },
    {
      start: start.setFullYear(2023, 6, 16),
      end: end.setFullYear(2023, 6, 23),
      number: "week6",
    },
    {
      start: start.setFullYear(2023, 6, 23),
      end: end.setFullYear(2023, 6, 30),
      number: "week7",
    },
    {
      start: start.setFullYear(2023, 6, 30),
      end: end.setFullYear(2023, 7, 6),
      number: "week8",
    },
    {
      start: start.setFullYear(2023, 7, 6),
      end: end.setFullYear(2023, 7, 13),
      number: "week9",
    },
    {
      start: start.setFullYear(2023, 7, 13),
      end: end.setFullYear(2023, 7, 20),
      number: "week10",
    },
    {
      start: start.setFullYear(2023, 7, 20),
      end: end.setFullYear(2023, 7, 27),
      number: "week11",
    },
  ];
  let weekNumber;
  for (let i = 0; i < week.length; i++) {
    if (week[i].start < date && date < week[i].end) {
      weekNumber = week[i].number;
    }
  }
  const hearDate = {
    year: date.getFullYear(),
    month: date.getMonth() + 1,
    day: date.getDate(),
    hour: date.getHours(),
    minute: date.getMinutes(),
    seconds: date.getSeconds(),
  };
  const dateHear = `${hearDate.day}/${hearDate.month}/${hearDate.year}`;
  const timeHear = `${hearDate.hour}:${hearDate.minute}:${hearDate.seconds}`;
  let point = 0;
  if (req.body.page != 0) {
    point = 3;
  } else if (req.body.ama != null) {
    const AmaPoint = await collection.findOne({ name: req.body.ama });
    point = AmaPoint.point;
  } else if (req.body.tabark != null) {
    const tabarkPoint = await collection.findOne({ name: req.body.tabark });
    point = tabarkPoint.point;
  }
  let rate;
  if (req.body.rate == 3) {
    rate = "ممتاز";
  } else if (req.body.rate == 2) {
    rate = "جيد جدا";
  } else {
    rate = "جيد";
  }

  collection = db.collection("hear");
  const new_hear = await collection.insertOne({
    name: req.body.name,
    rate: rate,
    point: point,
    dateHear: dateHear,
    timeHear: timeHear,
    date: date,
  });
  collection = db.collection("hear_week");
  const student = await collection.findOne({ name: req.body.name });
  if (weekNumber == "week0") {
    const weekPoint = student.point.week0 + point;
    const weekPointUpdate = await collection.updateOne(
      { name: req.body.name },
      { $set: { "point.week0": weekPoint } }
    );
  } else if (weekNumber == "week1") {
    const weekPoint = student.point.week1 + point;
    const weekPointUpdate = await collection.updateOne(
      { name: req.body.name },
      { $set: { "point.week1": weekPoint } }
    );
  } else if (weekNumber == "week2") {
    const weekPoint = student.point.week2 + point;
    const weekPointUpdate = await collection.updateOne(
      { name: req.body.name },
      { $set: { "point.week2": weekPoint } }
    );
  } else if (weekNumber == "week3") {
    const weekPoint = student.point.week3 + point;
    const weekPointUpdate = await collection.updateOne(
      { name: req.body.name },
      { $set: { "point.week3": weekPoint } }
    );
  } else if (weekNumber == "week4") {
    const weekPoint = student.point.week4 + point;
    const weekPointUpdate = await collection.updateOne(
      { name: req.body.name },
      { $set: { "point.week4": weekPoint } }
    );
  } else if (weekNumber == "week5") {
    const weekPoint = student.point.week5 + point;
    const weekPointUpdate = await collection.updateOne(
      { name: req.body.name },
      { $set: { "point.week5": weekPoint } }
    );
  } else if (weekNumber == "week6") {
    const weekPoint = student.point.week6 + point;
    const weekPointUpdate = await collection.updateOne(
      { name: req.body.name },
      { $set: { "point.week6": weekPoint } }
    );
  } else if (weekNumber == "week7") {
    const weekPoint = student.point.week7 + point;
    const weekPointUpdate = await collection.updateOne(
      { name: req.body.name },
      { $set: { "point.week7": weekPoint } }
    );
  } else if (weekNumber == "week8") {
    const weekPoint = student.point.week8 + point;
    const weekPointUpdate = await collection.updateOne(
      { name: req.body.name },
      { $set: { "point.week8": weekPoint } }
    );
  } else if (weekNumber == "week9") {
    const weekPoint = student.point.week9 + point;
    const weekPointUpdate = await collection.updateOne(
      { name: req.body.name },
      { $set: { "point.week9": weekPoint } }
    );
  } else if (weekNumber == "week10") {
    const weekPoint = student.point.week10 + point;
    const weekPointUpdate = await collection.updateOne(
      { name: req.body.name },
      { $set: { "point.week10": weekPoint } }
    );
  } else if (weekNumber == "week11") {
    const weekPoint = student.point.week11 + point;
    const weekPointUpdate = await collection.updateOne(
      { name: req.body.name },
      { $set: { "point.week11": weekPoint } }
    );
  }
  collection = db.collection("student");
  const studentPoint = await collection.findOne({ name: req.body.name });
  const allPoint = studentPoint.allPoint + point;
  console.log(studentPoint.allPoint);
  const allPointUpdate = await collection.updateOne(
    { name: req.body.name },
    { $set: { allPoint: allPoint } }
  );
  res.status(201).json({ mes: "sucsses" });
}
