// import Db from "@/utils/db";

// // Next.js API route support: https://nextjs.org/docs/api-routes/introduction

// export default async function handler(req, res) {
//   const client = await Db();
//   const db = client.db("ali");

//   res.status(200).json({ name: "John Doe" });
// }

function MyDate(req, res) {
  const date = new Date();
  const start = new Date();
  start.setHours(3, 0, 0, 0);
  start.setFullYear(2023, 5, 4);
  const end = new Date();
  end.setHours(3, 0, 0, 0);
  end.setFullYear(2023, 5, 11);
  const week = [
    {
      start: start.setFullYear(2023, 5, 4),
      end: end.setFullYear(2023, 5, 11),
      number: 0,
    },
    {
      start: start.setFullYear(2023, 5, 11),
      end: end.setFullYear(2023, 5, 18),
      number: 1,
    },
  ];
  let weekNumber;
  for (let i = 0; i < week.length; i++) {
    if (week[i].start < date && date < week[i].end) {
      weekNumber = week[i].number;
    }
  }
  res.status(200).json({ mes: weekNumber });
}
export default MyDate;
