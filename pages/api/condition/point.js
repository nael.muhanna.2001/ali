import Db from "@/utils/db";

export default async function handler(req, res) {
  const ama_point = {
    suraName: req.body.suraName,
    suraPoint: Number(req.body.suraPoint),
  };
  const client = await Db();
  const db = client.db("ali");
  const collection = db.collection("point_ama_tabark");
  const newStudent = await collection.insertOne({
    name: ama_point.suraName,
    point: ama_point.suraPoint,
  });
  res.status(201).json({ message: "Done!" });
}
