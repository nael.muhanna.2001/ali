import Db from "@/utils/db";
async function GetStudentData(req, res) {
  const client = await Db();
  const db = client.db("ali");
  const collection = db.collection("point_ama_tabark");
  const getData = await collection.find({}).toArray();
  res.status(200).json({ getData });
}
export default GetStudentData;
