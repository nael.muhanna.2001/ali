import Db from "@/utils/db";
async function GetStudentData(req, res) {
  const client = await Db();
  const db = client.db("ali");
  const collection = db.collection("student");
  let getData = await collection.find({}).sort({ allPoint: -1 }).toArray();
  for (let i = 0; i < getData.length; i++) {
    getData[i].order = i + 1;
  }
  res.status(200).json({ getData });
}
export default GetStudentData;
