import Db from "@/utils/db";
import { ObjectId } from "mongodb";
export default async function UpdateStudentData(req, res) {
  const client = await Db();
  const db = client.db("ali");
  const collection = db.collection("student");
  const studentId = req.query.update_delete_student;
  if (req.method === "PUT") {
    try {
      const studentUpdateData = await collection.updateOne(
        {
          _id: new ObjectId(studentId),
        },
        {
          $set: {
            name: req.body.name,
            calss: req.body.class,
            teacher: req.body.teacher,
            page: req.body.page,
            ama: req.body.ama,
            tabark: req.body.tabark,
            url_image: req.body.url_image,
          },
        }
      );
      res.status(201).json({ data: "UPDATED!" });
    } catch (error) {
      return res.status(400).json({ message: "Sorry something went wrong !" });
    }
  } else if (req.method === "DELETE") {
    try {
      const studentUpdateData = await collection.deleteOne({
        _id: new ObjectId(studentId),
      });
      res.status(201).json({ data: "DELETED!" });
    } catch (error) {
      return res.status(400).json({ message: "Sorry something went wrong !" });
    }
  } else {
    res.status(200).json({ data: "you have a problem!" });
  }
}
