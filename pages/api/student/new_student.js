import Db from "@/utils/db";
export default async function handler(req, res) {
  const client = await Db();
  const db = client.db("ali");
  let collection = db.collection("student");
  const newStudent = await collection.insertOne({
    name: req.body.name,
    calss: req.body.calss,
    teacher: req.body.teacher,
    page: req.body.page,
    ama: req.body.ama,
    tabark: req.body.tabark,
    allPoint: 0,
    order: 0,
    url_image: "",
  });
  collection = db.collection("hear_week");
  const newHearWeek = await collection.insertOne({
    name: req.body.name,
    point: {
      week0: 0,
      week1: 0,
      week2: 0,
      week3: 0,
      week4: 0,
      week5: 0,
      week6: 0,
      week7: 0,
      week8: 0,
      week9: 0,
      week10: 0,
      week11: 0,
    },
    level: {
      level0: 4,
      level1: 4,
      level2: 4,
      level3: 4,
      level4: 4,
      level5: 4,
      level6: 4,
      level7: 4,
      level8: 4,
      level9: 4,
      level10: 4,
      level11: 4,
    },
    star: {
      monthStar1: 0,
      monthStar2: 0,
      monthStar3: 0,
    },
  });
  res.status(201).json({ mes: "succses" });
}
