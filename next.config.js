/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  test: /\^(?!tailwind.css).(le|c)ss$/,
};

module.exports = nextConfig;
