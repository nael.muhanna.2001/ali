const { MongoClient } = require("mongodb");
const url = "mongodb://0.0.0.0:27017";
const client = new MongoClient(url);
async function Db() {
  try {
    await client.connect();
    console.log("Connected successfully to server!");
    setTimeout(() => {
      client.close();
    }, 30000);
    return client;
  } catch (error) {
    console.log(error);
  }
}
export default Db;
