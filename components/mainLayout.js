import MyNavbar from "../components/navbar";
function MainLayout(props) {
  return <MyNavbar>{props.children}</MyNavbar>;
}
export default MainLayout;
