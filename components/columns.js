export const COLUMNS = [
  {
    Header: "الترتيب",
  },
  {
    Header: "الاسم",
    accessor: "student_name",
  },
  {
    Header: "الصف",
    accessor: "student_calss",
  },
  {
    Header: "اسم الاستاذ",
    accessor: "teacher_name",
  },
  {
    Header: "رقم الجزء",
    accessor: "part_number",
  },
  {
    Header: "عم",
    accessor: "ama",
  },
  {
    Header: "تبارك",
    accessor: "tabark",
  },
  {
    Header: "المستوى",
    accessor: "student_level",
  },
  {
    Header: "النقاط",
  },
];
