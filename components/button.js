import { Button, ButtonToolbar, ButtonGroup, IconButton } from "rsuite";
const CustomButtonGroup = ({ appearance }) => (
  <ButtonGroup justified>
    <Button
      appearance={appearance}
      className=" h-40 font-bold !bg-amber-400 !text-black !text-4xl"
    >
      المستوى الأول
    </Button>
    <Button
      appearance={appearance}
      className=" h-40 font-bold !bg-red-700 !text-black !text-4xl"
    >
      المستوى الثاني
    </Button>
    <Button
      appearance={appearance}
      className="h-40 font-bold !bg-amber-400 !text-black !text-4xl"
    >
      المستوى الثالث
    </Button>
    <Button
      appearance={appearance}
      className=" h-40 font-bold !bg-red-700 !text-black !text-4xl"
    >
      المستوى الرابع
    </Button>
  </ButtonGroup>
);
export default function MyButton() {
  return (
    <>
      <div className="m-10 mt-60">
        <CustomButtonGroup />
      </div>
    </>
  );
}
