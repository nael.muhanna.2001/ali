import { Table, Avatar } from "rsuite";
const { Column, HeaderCell, Cell } = Table;
const ImageCell = ({ rowData, dataKey, ...props }) => (
  <Cell {...props} style={{ padding: 0 }}>
    <Avatar src={rowData.url_image}></Avatar>
  </Cell>
);
export default function MyTable({ student }) {
  return (
    <div className=" m-8">
      <div className=" text-center m-4">
        <h4 className=" font-light">جدول الترتيب الكلي</h4>
      </div>
      <div className=" !mr-40">
        <Table data={student} width={1180} height={550}>
          <Column align="center" fixed width={100}>
            <HeaderCell className=" text-white font-bold font-2xl">
              الترتيب
            </HeaderCell>
            <Cell dataKey="order" />
          </Column>
          <Column width={120} align="center">
            <HeaderCell className=" text-white font-bold font-2xl">
              الصورة
            </HeaderCell>
            <ImageCell dataKey="url_image" />
          </Column>
          <Column align="center" fixed width={240}>
            <HeaderCell className=" text-white font-bold font-2xl">
              الطالب
            </HeaderCell>

            <Cell dataKey="name" />
          </Column>
          <Column align="center" fixed width={240}>
            <HeaderCell className=" text-white font-bold font-2xl">
              الصف
            </HeaderCell>
            <Cell dataKey="calss" />
          </Column>
          <Column align="center" fixed width={240}>
            <HeaderCell className=" text-white font-bold font-2xl">
              الأستاذ
            </HeaderCell>
            <Cell dataKey="teacher" />
          </Column>
          <Column align="center" fixed width={240}>
            <HeaderCell className=" text-white font-bold font-2xl">
              النقاط
            </HeaderCell>
            <Cell dataKey="allPoint" />
          </Column>
        </Table>
      </div>
    </div>
  );
}
