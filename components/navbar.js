import { Navbar, Nav } from "rsuite";
import HomeIcon from "@rsuite/icons/legacy/Home";
import Link from "next/link";
function MyNavbar() {
  return (
    <Navbar appearance="inverse">
      <Navbar.Brand href="">RSUITE</Navbar.Brand>
      <Nav>
        <Nav.Item icon={<HomeIcon />} href="/">
          الصفحة الرئيسية
        </Nav.Item>
        <Nav.Item href="/student/new">طالب جديد</Nav.Item>
        <Nav.Item href="/hear">تسميع جديد</Nav.Item>
        <Nav.Item href="/student/all">نقاط كل الطلاب</Nav.Item>
        <Nav.Item href="/level"> المستويات </Nav.Item>
        <Nav.Item href="/condition/getPoint"> النقاط </Nav.Item>
      </Nav>
    </Navbar>
  );
}
export default MyNavbar;
